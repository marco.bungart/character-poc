package com.turing.nextrpg.dice.model;

import java.util.Objects;
import java.util.Random;

public class FairDice {
  private static final Random RANDOM = new Random();

  private final int sides;

  private FairDice(final int sides) {
    this.sides = sides;
  }

  public static FairDice of(final int sides) {
    return new FairDice(sides);
  }

  public final int sides() {
    return sides;
  }

  public final int roll() {
    return RANDOM.nextInt(sides) + 1;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final FairDice fairDice = (FairDice) o;
    return sides == fairDice.sides;
  }

  @Override
  public int hashCode() {
    return Objects.hash(sides);
  }
}
