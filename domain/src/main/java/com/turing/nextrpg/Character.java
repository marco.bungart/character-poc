package com.turing.nextrpg;

import com.turing.nextrpg.character.stats.model.Abilities;
import com.turing.nextrpg.character.stats.model.Ability;

public class Character {
  private final Abilities abilities;

  private Character(final Builder builder) {
    this.abilities = builder.abilities();
  }

  public Ability strength() {
    return abilities.strength();
  }

  public Ability dexterity() {
    return abilities.dexterity();
  }

  public Ability constitution() {
    return abilities.constitution();
  }

  public Ability intelligence() {
    return abilities.intelligence();
  }

  public Ability wisdom() {
    return abilities.wisdom();
  }

  public Ability charisma() {
    return abilities.charisma();
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    private Abilities abilities = null;

    public Builder withAbilities(final Abilities abilities) {
      this.abilities = abilities;
      return this;
    }

    private Abilities abilities() {
      return abilities;
    }

    public Character build() {
      return new Character(this);
    }
  }
}
