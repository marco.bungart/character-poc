package com.turing.nextrpg;

import com.turing.nextrpg.roll.model.RollComponent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Roll {
  private ArrayList<RollComponent> rollComponents = new ArrayList<>();

  public List<RollComponent> components() {
    return Collections.unmodifiableList(rollComponents);
  }

  public void addComponent(final RollComponent rollComponent) {
    rollComponents.add(rollComponent);
  }

  public void addComponents(final Collection<? extends RollComponent> components) {
    this.rollComponents.addAll(components);
  }

  /**
   * Returns the result of this roll.
   *
   * <p>This method must call {@link RollComponent#value()} on all {@link RollComponent}s that were
   * added through {@link #addComponent(RollComponent)} or {@link #addComponents(Collection)}
   *
   * @return the commulative result of the roll.
   */
  public int result() {
    return rollComponents.stream()
        .mapToInt(RollComponent::value)
        .sum();
  }
}
