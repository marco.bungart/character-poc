package com.turing.nextrpg.character.stats.model;

public class Abilities {
  private final Ability strength;
  private final Ability dexterity;
  private final Ability constitution;
  private final Ability intelligence;
  private final Ability wisdom;
  private final Ability charisma;

  private Abilities(final Builder builder) {
    strength = Ability.strength(builder.strengthScore());
    dexterity = Ability.dexterity(builder.dexterityScore());
    constitution = Ability.constitution(builder.constitutionScore());
    intelligence = Ability.intelligence(builder.intelligenceScore());
    wisdom = Ability.wisdom(builder.wisdomScore());
    charisma = Ability.charisma(builder.charismaScore());
  }

  public static Builder builder() {
    return new Builder();
  }

  public Ability strength() {
    return strength;
  }

  public Ability dexterity() {
    return dexterity;
  }

  public Ability constitution() {
    return constitution;
  }

  public Ability intelligence() {
    return intelligence;
  }

  public Ability wisdom() {
    return wisdom;
  }

  public Ability charisma() {
    return charisma;
  }

  public static class Builder {
    int strengthScore;
    int dexterityScore;
    int constitutionScore;
    int intelligenceScore;
    int wisdomScore;
    int charismaScore;

    public Builder withStrength(final int score) {
      this.strengthScore = score;
      return this;
    }

    private int strengthScore() {
      return strengthScore;
    }

    public Builder withDexterity(final int score) {
      this.dexterityScore = score;
      return this;
    }

    private int dexterityScore() {
      return dexterityScore;
    }

    public Builder withConstitution(final int score) {
      this.constitutionScore = score;
      return this;
    }

    private int constitutionScore() {
      return constitutionScore;
    }

    public Builder withIntelligence(final int score) {
      this.intelligenceScore = score;
      return this;
    }

    private int intelligenceScore() {
      return intelligenceScore;
    }

    public Builder withWisdom(final int score) {
      this.wisdomScore = score;
      return this;
    }

    private int wisdomScore() {
      return wisdomScore;
    }

    public Builder withCharisma(final int score) {
      this.charismaScore = score;
      return this;
    }

    private int charismaScore() {
      return charismaScore;
    }

    public Abilities build() {
      return new Abilities(this);
    }
  }
}
