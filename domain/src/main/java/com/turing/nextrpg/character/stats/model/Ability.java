package com.turing.nextrpg.character.stats.model;

import java.util.Objects;

public class Ability {
  private final AbilityType abilityType;
  private final int score;

  private Ability(final AbilityType abilityType, final int score) {
    this.abilityType = abilityType;
    this.score = score;
  }

  public static Ability strength(final int score) {
    return new Ability(AbilityType.STRENGTH, score);
  }

  public static Ability dexterity(final int score) {
    return new Ability(AbilityType.DEXTERITY, score);
  }

  public static Ability constitution(final int score) {
    return new Ability(AbilityType.CONSTITUTION, score);
  }

  public static Ability intelligence(final int score) {
    return new Ability(AbilityType.INTELLIGENCE, score);
  }

  public static Ability wisdom(final int score) {
    return new Ability(AbilityType.WISDOM, score);
  }

  public static Ability charisma(final int score) {
    return new Ability(AbilityType.CHARISMA, score);
  }

  public final int score() {
    return score;
  }

  public final String name() {
    return abilityType.name();
  }

  public final String shortHand() {
    return abilityType.shortHand();
  }

  public final String description() {
    return abilityType.description();
  }

  @Override public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final Ability ability = (Ability) o;
    return score == ability.score && abilityType.equals(ability.abilityType);
  }

  @Override public int hashCode() {
    return Objects.hash(abilityType, score);
  }
}
