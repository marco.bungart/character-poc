package com.turing.nextrpg.character.stats.model;

import java.util.Objects;

// TODO: this is a shitty name. Come up with something more descriptive.
public class AbilityType {

  public static final AbilityType STRENGTH = new AbilityType("strength", "strength lorem ipsum");
  public static final AbilityType DEXTERITY = new AbilityType("dexterity", "dexterity lorem ipsum");
  public static final AbilityType CONSTITUTION =
      new AbilityType("constitution", "constitution lorem ipsum");
  public static final AbilityType INTELLIGENCE =
      new AbilityType("intelligence", "intelligence lorem ipsum");
  public static final AbilityType WISDOM = new AbilityType("wisdom", "wisdom lorem ipsum");
  public static final AbilityType CHARISMA = new AbilityType("charisma", "charisma lorem ipsum");

  private final String name;
  private final String description;

  private AbilityType(final String name, final String description) {
    this.name = name;
    this.description = description;
  }

  public final String name() {
    return name;
  }

  public final String description() {
    return description;
  }

  public final String shortHand() {
    return name().substring(0, 3).toUpperCase();
  }

  @Override public boolean equals(final Object o) {
    return this == o;
  }

  @Override public int hashCode() {
    return Objects.hash(name);
  }
}
