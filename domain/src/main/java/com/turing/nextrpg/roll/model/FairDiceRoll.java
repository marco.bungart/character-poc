package com.turing.nextrpg.roll.model;

import com.turing.nextrpg.dice.model.FairDice;
import java.util.Objects;

public class FairDiceRoll implements RollComponent {
  private final int sign;
  private final FairDice die;
  private volatile Integer rollResult;

  private FairDiceRoll(final int sign, final FairDice die) {
    this.sign = sign;
    this.die = die;
    this.rollResult = null;
  }

  public static FairDiceRoll of(final int sides) {
    return of(1, sides);
  }

  public static FairDiceRoll of(final int sign, final int sides) {
    return new FairDiceRoll(sign < 0 ? -1 : 1, FairDice.of(sides));
  }

  public int sign() {
    return sign;
  }

  public int sides() {
    return die.sides();
  }

  @Override
  public int value() {
    if (rollResult == null) {
      rollResult = sign * die.roll();
    }
    return rollResult;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final FairDiceRoll that = (FairDiceRoll) o;
    return sign == that.sign && die.equals(that.die);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sign, die);
  }
}
