package com.turing.nextrpg.roll.model;

public interface RollComponent {
  int value();
}
