package com.turing.nextrpg.roll.model;

import java.util.Objects;

public class ConstantRollComponent implements RollComponent {
  private final int value;

  private ConstantRollComponent(final int value) {
    this.value = value;
  }

  public static ConstantRollComponent of(final int value) {
    return new ConstantRollComponent(value);
  }

  @Override
  public final int value() {
    return value;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final ConstantRollComponent that = (ConstantRollComponent) o;
    return value == that.value;
  }

  @Override
  public int hashCode() {
    return Objects.hash(value);
  }
}
