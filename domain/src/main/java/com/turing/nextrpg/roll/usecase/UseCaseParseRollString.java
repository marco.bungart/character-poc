package com.turing.nextrpg.roll.usecase;

import com.turing.nextrpg.Roll;
import com.turing.nextrpg.roll.model.ConstantRollComponent;
import com.turing.nextrpg.roll.model.FairDiceRoll;
import com.turing.nextrpg.roll.model.RollComponent;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UseCaseParseRollString {
  private static final UseCaseParseRollString INSTANCE = new UseCaseParseRollString();
  private static final String LEGAL_ROLL_STRING_REGEX =
      "^\\s*"
          + "[+-]?(?:(?:d\\d+)|(?:\\d+(?:d\\d+)?))"
          + "\\s*"
          + "(?:\\s*[+-]\\s*(?:(?:d\\d+)|(?:\\d+(?:d\\d+)?)))*\\s*$";
  private static final String COMPONENT_EXTRACTOR_REGEX =
      // @formatter:off
      "(?:"
          + "(?<sign>[+-])\\s*"
          + "(?:"
              + "(?:(?<diceCount>\\d+)?(?<isDie>d)(?<sides>\\d+))"
              + "|"
              + "(?<constant>\\d+)"
          + ")"
      + ")";
  // @formatter:on

  private UseCaseParseRollString() {
    // Hide default constructor
  }

  public static boolean isLegalRollString(final String input) {
    return input.matches(LEGAL_ROLL_STRING_REGEX);
  }

  public static UseCaseParseRollString get() {
    return INSTANCE;
  }

  /**
   * This method parses a {@link String} into a {@link Roll}.
   *
   * @param input
   *     the {@link String}-representation of a roll
   *
   * @return an {@link Optional} holding:
   * <ul>
   *   <li>the resulting roll, if parsing was successful</li>
   *   <li>nothing if parsing was unsuccessful.</li>
   * </ul>
   */
  public Optional<Roll> parse(String input) {
    if (!isLegalRollString(input)) {
      return Optional.empty();
    }
    if (!input.matches("^[+-].*$")) {
      input = "+" + input;
    }
    final Roll roll = new Roll();
    final Matcher matcher = Pattern.compile(COMPONENT_EXTRACTOR_REGEX).matcher(input);
    roll.addComponents(extractComponents(matcher));

    return Optional.of(roll);
  }

  private List<RollComponent> extractComponents(Matcher matcher) {
    final ArrayList<RollComponent> rollComponents = new ArrayList<>();
    while (matcher.find()) {
      final boolean isDiceComponent = "d".equals(matcher.group("isDie"));
      final String signString = matcher.group("sign").trim();
      final int sign = "+".equals(signString) ? 1 : -1;
      if (isDiceComponent) {
        rollComponents.addAll(matchToDiceComponents(matcher, sign));
      } else {
        rollComponents.add(matchToConstantComponent(matcher, sign));
      }
    }

    return rollComponents;
  }

  private List<FairDiceRoll> matchToDiceComponents(Matcher matcher, int sign) {
    final ArrayList<FairDiceRoll> result = new ArrayList<>();
    final int diceCount = extractDiceCount(matcher);
    final String sidesString = matcher.group("sides").trim();
    final int sides = Integer.parseInt(sidesString);
    for (int i = 0; i < diceCount; ++i) {
      result.add(FairDiceRoll.of(sign, sides));
    }

    return result;
  }

  private int extractDiceCount(final Matcher matcher) {
    final Optional<String> diceCountString = Optional.ofNullable(matcher.group("diceCount"));
    return diceCountString.map(s -> Integer.parseInt(s.trim())).orElse(1);
  }

  private RollComponent matchToConstantComponent(Matcher matcher, int sign) {
    final String valueString = matcher.group("constant").trim();
    final int value = sign * Integer.parseInt(valueString);

    return ConstantRollComponent.of(value);
  }
}
