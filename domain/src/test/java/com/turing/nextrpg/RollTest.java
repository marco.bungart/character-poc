package com.turing.nextrpg;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.turing.nextrpg.roll.model.RollComponent;
import com.turing.nextrpg.roll.model.ConstantRollComponent;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

class RollTest {

  @Test
  void shouldConstructEmptyRoll() {
    // GIVEN:
    final int expectedSize = 0;
    final int expectedResult = 0;

    // WHEN:
    final Roll underTest = new Roll();

    // THEN:
    assertEquals(expectedSize, underTest.components().size());
    assertEquals(expectedResult, underTest.result());
  }

  @Test
  void shouldAddConstantComponent() {
    // GIVEN:
    final Roll underTest = new Roll();
    final RollComponent expectedRollComponent = mock(RollComponent.class);
    final int expectedSize = 1;

    // WHEN
    underTest.addComponent(expectedRollComponent);

    // THEN
    assertEquals(expectedSize, underTest.components().size());
    List<RollComponent> actualRollComponents = underTest.components();
    assertThat(actualRollComponents, contains(expectedRollComponent));
  }

  @Test
  void shouldAddConstantComponents() {
    // GIVEN:
    final List<RollComponent> expectedRollComponents =
        Arrays.asList(mock(ConstantRollComponent.class), mock(ConstantRollComponent.class), mock(
            ConstantRollComponent.class));
    final Roll underTest = new Roll();

    // WHEN
    underTest.addComponents(expectedRollComponents);

    // THEN
    List<RollComponent> actualRollComponents = underTest.components();
    assertThat(expectedRollComponents, containsInAnyOrder(actualRollComponents.toArray()));
    assertThat(actualRollComponents, containsInAnyOrder(expectedRollComponents.toArray()));
  }

  @Test
  void shouldCallValueOnAllComponents() {
    // GIVEN:
    final Roll underTest = new Roll();
    final List<RollComponent> rollComponents = IntStream.range(0, 10)
        .mapToObj(i -> mock(RollComponent.class))
        .collect(Collectors.toList());
    underTest.addComponents(rollComponents);

    // WHEN
    underTest.result();

    // THEN
    rollComponents.forEach(c -> verify(c).value());
  }

  @Test
  void shouldReturnExpectedResult() {
    // GIVEN:
    final int n = 10;
    final List<RollComponent> rollComponents = IntStream.range(0, n + 1)
        .mapToObj(value -> {
          RollComponent rollComponent = mock(RollComponent.class);
          when(rollComponent.value()).thenReturn(value);
          return rollComponent;
        })
        .collect(Collectors.toList());
    final Roll underTest = new Roll();
    underTest.addComponents(rollComponents);
    final int expectedResult = (n * (n + 1) / 2);

    // WHEN
    final int actualResult = underTest.result();

    // THEN
    assertEquals(expectedResult, actualResult);
  }
}