package com.turing.nextrpg.dice.model;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

class FairDiceTest {

  @Test
  void shouldConstructExpectedDie() {
    // GIVEN
    final int expectedSides = 6;

    // WHEN
    final FairDice dice = FairDice.of(expectedSides);

    // THEN
    assertEquals(expectedSides, dice.sides());
  }

  @Test
  void shouldProduceMeansNearExpectedMean() {
    // GIVEN
    final double expectedAverage = 3.5d;
    final FairDice dice = FairDice.of(6);

    // WHEN
    final double actualAverage = IntStream.range(0, 1_000_000)
        .map(i -> dice.roll())
        .summaryStatistics()
        .getAverage();

    // THEN
    assertThat(expectedAverage, closeTo(actualAverage, 0.01));
  }

  @Test
  void shouldReturnTrueWhenComparedToItself() {
    // GIVEN
    final FairDice underTest = FairDice.of(6);

    // WHEN
    final boolean isEqual = underTest.equals(underTest);

    // THEN
    assertTrue(isEqual);
  }

  @Test
  void shouldReturnTrueWhenComparedToEqualDices() {
    // GIVEN
    final FairDice underTest = FairDice.of(6);
    final FairDice equalFairDice = FairDice.of(6);

    // WHEN
    final boolean isEqual = underTest.equals(equalFairDice);

    // THEN
    assertTrue(isEqual);
  }

  @Test
  void shouldReturnFalseWhenComparedToNull() {
    // GIVEN
    final FairDice underTest = FairDice.of(6);

    // WHEN
    final boolean isEqual = underTest.equals(null);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnFalseWhenComparedToDifferentObjectType() {
    // GIVEN
    final FairDice underTest = FairDice.of(6);
    final Object differentTypeObject = new Object();

    // WHEN
    final boolean isEqual = underTest.equals(differentTypeObject);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnFalseWhenComparedToDifferentDices() {
    // GIVEN
    final FairDice underTest = FairDice.of(4);
    final FairDice differentFairDice = FairDice.of(6);

    // WHEN
    final boolean isEqual = underTest.equals(differentFairDice);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnSameHashCodeOnSameDices() {
    // GIVEN
    final FairDice underTest = FairDice.of(6);
    final FairDice equalFairDice = FairDice.of(6);

    // WHEN
    final long hashCodeOne = underTest.hashCode();
    final long hashCodeTwo = equalFairDice.hashCode();

    // THEN
    assertEquals(hashCodeOne, hashCodeTwo);
  }

  @Test
  void shouldReturnDifferentHashCodeOnUnequalDices() {
    // GIVEN
    final FairDice underTest = FairDice.of(4);
    final FairDice differentFairDice = FairDice.of(6);

    // WHEN
    final long hashCodeOne = underTest.hashCode();
    final long hashCodeTwo = differentFairDice.hashCode();

    // THEN
    assertNotEquals(hashCodeOne, hashCodeTwo);
  }
}