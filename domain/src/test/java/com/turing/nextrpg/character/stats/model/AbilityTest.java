package com.turing.nextrpg.character.stats.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class AbilityTest {

  @Test
  void shouldConstructExpectedStrengthAbility() {
    // GIVEN
    final AbilityType expectedAbilityType = AbilityType.STRENGTH;
    final int expectedScore = 10;

    // WHEN
    final Ability underTest = Ability.strength(expectedScore);

    // THEN
    assertEquals(expectedAbilityType.name(), underTest.name());
    assertEquals(expectedAbilityType.shortHand(), underTest.shortHand());
    assertEquals(expectedAbilityType.description(), underTest.description());
    assertEquals(expectedScore, underTest.score());
  }

  @Test
  void shouldConstructExpectedDexterityAbility() {
    // GIVEN
    final AbilityType expectedAbilityType = AbilityType.DEXTERITY;
    final int expectedScore = 10;

    // WHEN
    final Ability underTest = Ability.dexterity(expectedScore);

    // THEN
    assertEquals(expectedAbilityType.name(), underTest.name());
    assertEquals(expectedAbilityType.shortHand(), underTest.shortHand());
    assertEquals(expectedAbilityType.description(), underTest.description());
    assertEquals(expectedScore, underTest.score());
  }

  @Test
  void shouldConstructExpectedConstitutionAbility() {
    // GIVEN
    final AbilityType expectedAbilityType = AbilityType.CONSTITUTION;
    final int expectedScore = 10;

    // WHEN
    final Ability underTest = Ability.constitution(expectedScore);

    // THEN
    assertEquals(expectedAbilityType.name(), underTest.name());
    assertEquals(expectedAbilityType.shortHand(), underTest.shortHand());
    assertEquals(expectedAbilityType.description(), underTest.description());
    assertEquals(expectedScore, underTest.score());
  }

  @Test
  void shouldConstructExpectedIntelligenceAbility() {
    // GIVEN
    final AbilityType expectedAbilityType = AbilityType.INTELLIGENCE;
    final int expectedScore = 10;

    // WHEN
    final Ability underTest = Ability.intelligence(expectedScore);

    // THEN
    assertEquals(expectedAbilityType.name(), underTest.name());
    assertEquals(expectedAbilityType.shortHand(), underTest.shortHand());
    assertEquals(expectedAbilityType.description(), underTest.description());
    assertEquals(expectedScore, underTest.score());
  }

  @Test
  void shouldConstructExpectedWisdomAbility() {
    // GIVEN
    final AbilityType expectedAbilityType = AbilityType.WISDOM;
    final int expectedScore = 10;

    // WHEN
    final Ability underTest = Ability.wisdom(expectedScore);

    // THEN
    assertEquals(expectedAbilityType.name(), underTest.name());
    assertEquals(expectedAbilityType.shortHand(), underTest.shortHand());
    assertEquals(expectedAbilityType.description(), underTest.description());
    assertEquals(expectedScore, underTest.score());
  }

  @Test
  void shouldConstructExpectedCharismaAbility() {
    // GIVEN
    final AbilityType expectedAbilityType = AbilityType.CHARISMA;
    final int expectedScore = 10;

    // WHEN
    final Ability underTest = Ability.charisma(expectedScore);

    // THEN
    assertEquals(expectedAbilityType.name(), underTest.name());
    assertEquals(expectedAbilityType.shortHand(), underTest.shortHand());
    assertEquals(expectedAbilityType.description(), underTest.description());
    assertEquals(expectedScore, underTest.score());
  }

  @Test
  void shouldReturnTrueWhenAbilityIsComparedToItself() {
    // GIVEN
    final Ability underTest = Ability.strength(10);

    // WHEN
    final boolean isEqual = underTest.equals(underTest);

    // THEN
    assertTrue(isEqual);
  }

  @Test
  void shouldReturnTrueWhenComparedToEqualObject() {
    // GIVEN
    final Ability underTest = Ability.strength(10);
    final Ability equalAbility = Ability.strength(10);

    // WHEN
    boolean isEqual = underTest.equals(equalAbility);

    // THEN
    assertTrue(isEqual);
  }
  
  @Test
  void shouldReturnFalseWhenComparedToNull() {
    // GIVEN
    final Ability underTest = Ability.strength(10);

    // WHEN
    boolean isEqual = underTest.equals(null);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnFalseWhenComparedToAttributeOfDifferentKind() {
    // GIVEN
    final Ability underTest = Ability.strength(10);
    final Ability differentAbility = Ability.dexterity(10);

    // WHEN
    final boolean isEqual = underTest.equals(differentAbility);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnFalseWhenComparedToDifferentObjectType() {
    // GIVEN
    final Ability underTest = Ability.strength(10);
    final Object differentTypeObject = new Object();

    // WHEN
    final boolean isEqual = underTest.equals(differentTypeObject);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnFalseWhenComparedToAttributeWithDifferentScore() {
    // GIVEN
    final Ability underTest = Ability.strength(10);
    final Ability differentAbility = Ability.strength(11);

    // WHEN
    final boolean isEqual = underTest.equals(differentAbility);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnSameHashCodeOnEqualAbility() {
    // GIVEN
    final Ability underTest = Ability.strength(10);
    final Ability equalAbility = Ability.strength(10);

    // WHEN
    final long hashCodeOne = underTest.hashCode();
    final long hashCodeTwo = equalAbility.hashCode();

    // THEN
    assertEquals(hashCodeOne, hashCodeTwo);
  }

  @Test
  void shouldReturnDifferentHashCodeWhenAbilitiesDifferInScore() {
    // GIVEN
    final Ability underTest = Ability.strength(10);
    final Ability differentAbility = Ability.strength(11);

    // WHEN
    final long hashCodeOne = underTest.hashCode();
    final long hashCodeTwo = differentAbility.hashCode();

    // THEN
    assertNotEquals(hashCodeOne, hashCodeTwo);
  }

  @Test
  void shouldReturnDifferentHashCodeWhenAbilitiesDifferInType() {
    // GIVEN
    final Ability underTest = Ability.strength(10);
    final Ability differentAbility = Ability.dexterity(10);

    // WHEN
    final long hashCodeOne = underTest.hashCode();
    final long hashCodeTwo = differentAbility.hashCode();

    // THEN
    assertNotEquals(hashCodeOne, hashCodeTwo);
  }
}