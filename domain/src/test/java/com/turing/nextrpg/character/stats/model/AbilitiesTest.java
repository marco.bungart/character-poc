package com.turing.nextrpg.character.stats.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class AbilitiesTest {

  @Nested
  class AbilityBuilderTest {
    @Test
    void shouldReturnExpectedAbilities() {
      // GIVEN
      final int expectedStrengthScore = 10;
      final int expectedDexterityScore = 11;
      final int expectedConstitutionScore = 12;
      final int expectedIntelligenceScore = 13;
      final int expectedWisdomScore = 14;
      final int expectedCharismaScore = 15;

      // WHEN
      final Abilities actual = Abilities.builder()
          .withStrength(expectedStrengthScore)
          .withDexterity(expectedDexterityScore)
          .withConstitution(expectedConstitutionScore)
          .withIntelligence(expectedIntelligenceScore)
          .withWisdom(expectedWisdomScore)
          .withCharisma(expectedCharismaScore)
          .build();

      // THEN
      verifyAttributeHasExpectedValues(
          expectedStrengthScore,
          AbilityType.STRENGTH,
          actual.strength());
      verifyAttributeHasExpectedValues(
          expectedDexterityScore,
          AbilityType.DEXTERITY,
          actual.dexterity());
      verifyAttributeHasExpectedValues(
          expectedConstitutionScore,
          AbilityType.CONSTITUTION,
          actual.constitution());
      verifyAttributeHasExpectedValues(
          expectedIntelligenceScore,
          AbilityType.INTELLIGENCE,
          actual.intelligence());
      verifyAttributeHasExpectedValues(
          expectedWisdomScore,
          AbilityType.WISDOM,
          actual.wisdom());
      verifyAttributeHasExpectedValues(
          expectedCharismaScore,
          AbilityType.CHARISMA,
          actual.charisma());
    }

    private void verifyAttributeHasExpectedValues(
        final int expectedStrengthScore,
        final AbilityType expectedAbilityType,
        final Ability actualAbility) {
      assertEquals(expectedAbilityType.name(), actualAbility.name());
      assertEquals(expectedAbilityType.shortHand(), actualAbility.shortHand());
      assertEquals(expectedAbilityType.description(), actualAbility.description());
      assertEquals(expectedStrengthScore, actualAbility.score());
    }
  }
}