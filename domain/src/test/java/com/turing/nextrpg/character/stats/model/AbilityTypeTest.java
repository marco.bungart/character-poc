package com.turing.nextrpg.character.stats.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class AbilityTypeTest {

  @Test
  void assertStrengthHasRightName() {
    // GIVEN:
    final String expectedName = "strength";

    // WHEN
    String actualName = AbilityType.STRENGTH.name();

    // THEN
    assertEquals(expectedName, actualName);
  }

  @Test
  void assertDexterityHasRightName() {
    // GIVEN:
    final String expectedName = "dexterity";

    // WHEN
    String actualName = AbilityType.DEXTERITY.name();

    // THEN
    assertEquals(expectedName, actualName);
  }

  @Test
  void assertConstitutionHasRightName() {
    // GIVEN:
    final String expectedName = "constitution";

    // WHEN
    String actualName = AbilityType.CONSTITUTION.name();

    // THEN
    assertEquals(expectedName, actualName);
  }

  @Test
  void assertIntelligenceHasRightName() {
    // GIVEN:
    final String expectedName = "intelligence";

    // WHEN
    String actualName = AbilityType.INTELLIGENCE.name();

    // THEN
    assertEquals(expectedName, actualName);
  }

  @Test
  void assertWisdomHasRightName() {
    // GIVEN:
    final String expectedName = "wisdom";

    // WHEN
    String actualName = AbilityType.WISDOM.name();

    // THEN
    assertEquals(expectedName, actualName);
  }

  @Test
  void assertCharismaHasRightName() {
    // GIVEN:
    final String expectedName = "charisma";

    // WHEN
    String actualName = AbilityType.CHARISMA.name();

    // THEN
    assertEquals(expectedName, actualName);
  }

  @Test
  void assertStrengthHasRightShortHand() {
    // GIVEN:
    final String expectedShortHand = "STR";

    // WHEN
    String actualShortHand = AbilityType.STRENGTH.shortHand();

    // THEN
    assertEquals(expectedShortHand, actualShortHand);
  }

  @Test
  void assertDexterityHasRightShortHand() {
    // GIVEN:
    final String expectedShortHand = "DEX";

    // WHEN
    String actualShortHand = AbilityType.DEXTERITY.shortHand();

    // THEN
    assertEquals(expectedShortHand, actualShortHand);
  }

  @Test
  void assertConstitutionHasRightShortHand() {
    // GIVEN:
    final String expectedShortHand = "CON";

    // WHEN
    String actualShortHand = AbilityType.CONSTITUTION.shortHand();

    // THEN
    assertEquals(expectedShortHand, actualShortHand);
  }

  @Test
  void assertIntelligenceHasRightShortHand() {
    // GIVEN:
    final String expectedShortHand = "INT";

    // WHEN
    String actualShortHand = AbilityType.INTELLIGENCE.shortHand();

    // THEN
    assertEquals(expectedShortHand, actualShortHand);
  }

  @Test
  void assertWisdomHasRightShortHand() {
    // GIVEN:
    final String expectedShortHand = "WIS";

    // WHEN
    String actualShortHand = AbilityType.WISDOM.shortHand();

    // THEN
    assertEquals(expectedShortHand, actualShortHand);
  }

  @Test
  void assertCharismaHasRightShortHand() {
    // GIVEN:
    final String expectedShortHand = "CHA";

    // WHEN
    String actualShortHand = AbilityType.CHARISMA.shortHand();

    // THEN
    assertEquals(expectedShortHand, actualShortHand);
  }

  @Test
  void assertStrengthHasRightDescription() {
    // GIVEN:
    final String expectedDescription = "strength lorem ipsum";

    // WHEN
    String actualDescription = AbilityType.STRENGTH.description();

    // THEN
    assertEquals(expectedDescription, actualDescription);
  }

  @Test
  void assertDexterityHasRightDescription() {
    // GIVEN:
    final String expectedDescription = "dexterity lorem ipsum";

    // WHEN
    String actualDescription = AbilityType.DEXTERITY.description();

    // THEN
    assertEquals(expectedDescription, actualDescription);
  }

  @Test
  void assertConstitutionHasRightDescription() {
    // GIVEN:
    final String expectedDescription = "constitution lorem ipsum";

    // WHEN
    String actualDescription = AbilityType.CONSTITUTION.description();

    // THEN
    assertEquals(expectedDescription, actualDescription);
  }

  @Test
  void assertIntelligenceHasRightDescription() {
    // GIVEN:
    final String expectedDescription = "intelligence lorem ipsum";

    // WHEN
    String actualDescription = AbilityType.INTELLIGENCE.description();

    // THEN
    assertEquals(expectedDescription, actualDescription);
  }

  @Test
  void assertWisdomHasRightDescription() {
    // GIVEN:
    final String expectedDescription = "wisdom lorem ipsum";

    // WHEN
    String actualDescription = AbilityType.WISDOM.description();

    // THEN
    assertEquals(expectedDescription, actualDescription);
  }

  @Test
  void assertCharismaHasRightDescription() {
    // GIVEN:
    final String expectedDescription = "charisma lorem ipsum";

    // WHEN
    String actualDescription = AbilityType.CHARISMA.description();

    // THEN
    assertEquals(expectedDescription, actualDescription);
  }

  @Test
  void shouldReturnTrueWhenTypeIsComparedToItself() {
    // GIVEN: nothing

    // WHEN
    final boolean isEqual = AbilityType.STRENGTH.equals(AbilityType.STRENGTH);

    // THEN
    assertTrue(isEqual);
  }

  @Test
  void shouldReturnFalseWhenTypeIsComparedToNull() {
    // GIVEN: nothing

    // WHEN
    final boolean isEqual = AbilityType.STRENGTH.equals(null);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnFalseWhenTwoDifferentTypesAreCompared() {
    // GIVEN: nothing

    // WHEN
    final boolean isEqual = AbilityType.STRENGTH.equals(AbilityType.DEXTERITY);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnDifferentHashCodesForDifferentTypes() {
    // GIVEN: nothing

    // WHEN
    final int hashCodeStrength = AbilityType.STRENGTH.hashCode();
    final int hashCodeDexterity = AbilityType.DEXTERITY.hashCode();

    // THEN
    assertNotEquals(hashCodeStrength, hashCodeDexterity);
  }
}