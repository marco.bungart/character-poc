package com.turing.nextrpg;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.turing.nextrpg.character.stats.model.Abilities;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class CharacterTest {

  @Nested
  public class CharacterBuilderTest {
    @Test
    void shouldCreateCharacterWithExpectedAbilities() {
      // GIVEN
      final Abilities mockedAbilities = mock(Abilities.class);

      // WHEN
      final Character character = Character.builder()
          .withAbilities(mockedAbilities)
          .build();
      character.strength();
      character.dexterity();
      character.constitution();
      character.intelligence();
      character.wisdom();
      character.charisma();

      // THEN
      verify(mockedAbilities).strength();
      verify(mockedAbilities).dexterity();
      verify(mockedAbilities).constitution();
      verify(mockedAbilities).intelligence();
      verify(mockedAbilities).wisdom();
      verify(mockedAbilities).charisma();
    }
  }
}