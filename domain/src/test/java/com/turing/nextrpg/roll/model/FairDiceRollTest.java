package com.turing.nextrpg.roll.model;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

class FairDiceRollTest extends RollComponentTest<FairDiceRoll> {

  @Override
  FairDiceRoll createUnderTest() {
    return FairDiceRoll.of(6);
  }

  @Test
  void shouldConstructExpectedFairDiceRollComponent() {
    // GIVEN
    final int expectedSign = 1;
    final int expectedSides = 6;

    // WHEN
    final FairDiceRoll underTest = FairDiceRoll.of(expectedSides);

    // THEN
    assertEquals(expectedSign, underTest.sign());
    assertEquals(expectedSides, underTest.sides());
  }

  @Test
  void shouldConstructExpectedFairDiceRollComponentIfSignIsNotOneOrMinusOne() {
    // GIVEN
    final int signParameter = -100;
    final int expectedSign = -1;
    final int expectedSides = 6;

    // WHEN
    final FairDiceRoll underTest = FairDiceRoll.of(signParameter, expectedSides);

    // THEN
    assertEquals(expectedSign, underTest.sign());
    assertEquals(expectedSides, underTest.sides());
  }

  @Test
  void shouldProduceMeansNearExpectedMean() {
    // GIVEN
    final double expectedAverage = 3.5d;

    // WHEN
    final double actualAverage = IntStream.range(0, 10_000_000)
        .map(i -> FairDiceRoll.of(6).value())
        .summaryStatistics()
        .getAverage();

    // THEN
    assertThat(expectedAverage, closeTo(actualAverage, 0.01d));
  }

  @Test
  void shouldReturnTrueWhenComparedToSelf() {
    // GIVEN
    final FairDiceRoll underTest = FairDiceRoll.of(6);

    // WHEN
    final boolean isEqual = underTest.equals(underTest);

    // THEN
    assertTrue(isEqual);
  }

  @Test
  void shouldReturnTrueWhenComparedToEqualComponent() {
    // GIVEN
    final FairDiceRoll underTest = FairDiceRoll.of(6);
    final FairDiceRoll equalFairDiceComponent = FairDiceRoll.of(6);

    // WHEN
    final boolean isEqual = underTest.equals(equalFairDiceComponent);

    // THEN
    assertTrue(isEqual);
  }

  @Test
  void shouldReturnFalseWhenComparedToNull() {
    // GIVEN
    final FairDiceRoll underTest = FairDiceRoll.of(6);

    // WHEN
    final boolean isEqual = underTest.equals(null);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnFalseWhenComparedToDifferentObjectType() {
    // GIVEN
    final FairDiceRoll underTest = FairDiceRoll.of(6);
    final Object differentTypeObject = new Object();

    // WHEN
    final boolean isEqual = underTest.equals(differentTypeObject);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnFalseWhenComparedToUnequalComponentDifferInSign() {
    // GIVEN
    final FairDiceRoll underTest = FairDiceRoll.of(6);
    final FairDiceRoll differentFairDiceComponent = FairDiceRoll.of(-1, 6);

    // WHEN
    final boolean isEqual = underTest.equals(differentFairDiceComponent);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnFalseWhenComparedToUnequalComponentDifferInDie() {
    // GIVEN
    final FairDiceRoll fairDice = FairDiceRoll.of(6);
    final FairDiceRoll differentDiceComponent = FairDiceRoll.of(4);

    // WHEN
    final boolean isEqual = fairDice.equals(differentDiceComponent);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnSameHashCodeOnEqualObject() {
    // GIVEN
    final FairDiceRoll underTest = FairDiceRoll.of(6);
    final FairDiceRoll equalFairDiceComponent = FairDiceRoll.of(6);

    // WHEN
    final int hashCodeOne = underTest.hashCode();
    final int hashCodeTwo = equalFairDiceComponent.hashCode();

    // THEN
    assertEquals(hashCodeOne, hashCodeTwo);
  }

  @Test
  void shouldReturnDifferentHashCodeOnUnequalComponent() {
    // GIVEN
    final FairDiceRoll underTest = FairDiceRoll.of(6);
    final FairDiceRoll differentFairDiceComponent = FairDiceRoll.of(-1, 6);

    // WHEN
    final int hashCodeOne = underTest.hashCode();
    final int hashCodeTwo = differentFairDiceComponent.hashCode();

    // THEN
    assertNotEquals(hashCodeOne, hashCodeTwo);
  }
}