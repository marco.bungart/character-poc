package com.turing.nextrpg.roll.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

abstract class RollComponentTest<R extends RollComponent> {
  abstract R createUnderTest();

  @Test
  void shouldReturnSameResultOnConsecutiveCalls() {
    // GIVEN
    final R underTest = createUnderTest();

    // WHEN
    final int expectedValue = underTest.value();

    // THEN
    IntStream.range(0, 1_000)
        .map(roll -> underTest.value())
        .forEach(actualValue -> assertEquals(expectedValue, actualValue));
  }
}