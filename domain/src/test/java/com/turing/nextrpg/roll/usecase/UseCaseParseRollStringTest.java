package com.turing.nextrpg.roll.usecase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.turing.nextrpg.Roll;
import com.turing.nextrpg.roll.model.RollComponent;
import com.turing.nextrpg.roll.model.ConstantRollComponent;
import com.turing.nextrpg.roll.model.FairDiceRoll;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;

class UseCaseParseRollStringTest {

  @Test
  void shouldRecogniseLegalStringSuccessfully() {
    // GIVEN
    final String legalString = "d6 + 5 + 2d4 + d5";

    // WHEN
    final boolean actual = UseCaseParseRollString.isLegalRollString(legalString);

    // THEN
    assertTrue(actual);
  }

  @Test
  void shouldRecogniseIllegalStringSuccessfully() {
    // GIVEN
    final String illegalString = "23 ++ 27";

    // WHEN
    final boolean actual = UseCaseParseRollString.isLegalRollString(illegalString);

    // THEN
    assertFalse(actual);
  }

  @Test
  void shouldGenerateExpectedRollIfFirstChar() {
    // GIVEN
    final String legalString = "-d6 + 5 -2d4 + d5 -3";
    final int expectedComponentSize = 6;
    final List<RollComponent> expectedRollComponents = Arrays.asList(
        FairDiceRoll.of(-1, 6),
        ConstantRollComponent.of(5),
        FairDiceRoll.of(-1, 4),
        FairDiceRoll.of(-1, 4),
        FairDiceRoll.of(5),
        ConstantRollComponent.of(-3)
    );

    // WHEN
    final Optional<Roll> result = UseCaseParseRollString.get().parse(legalString);

    // THEN
    assertTrue(result.isPresent());
    final Roll roll = result.get();
    assertEquals(expectedComponentSize, roll.components().size());
    final List<RollComponent> actualRollComponents = roll.components();
    assertThat(expectedRollComponents, containsInAnyOrder(actualRollComponents.toArray()));
    assertThat(actualRollComponents, containsInAnyOrder(expectedRollComponents.toArray()));
  }

  @Test
  void shouldGenerateExpectedRollIfFirstCharIsNotSign() {
    // GIVEN
    final String legalString = "d6 + 5 -2d4 + d5 -3";
    final int expectedComponentSize = 6;
    final List<RollComponent> expectedRollComponents = Arrays.asList(
        FairDiceRoll.of(6),
        ConstantRollComponent.of(5),
        FairDiceRoll.of(-1, 4),
        FairDiceRoll.of(-1, 4),
        FairDiceRoll.of(5),
        ConstantRollComponent.of(-3)
    );

    // WHEN
    final Optional<Roll> result = UseCaseParseRollString.get().parse(legalString);

    // THEN
    assertTrue(result.isPresent());
    final Roll roll = result.get();
    assertEquals(expectedComponentSize, roll.components().size());
    final List<RollComponent> actualRollComponents = roll.components();
    assertThat(expectedRollComponents, containsInAnyOrder(actualRollComponents.toArray()));
    assertThat(actualRollComponents, containsInAnyOrder(expectedRollComponents.toArray()));
  }

  @Test
  void shouldReturnEmptyOptionalOnIllegalString() {
    // GIVEN
    final String illegalString = "23 ++ 27";

    // WHEN
    final Optional<Roll> result = UseCaseParseRollString.get().parse(illegalString);

    // THEN
    assertFalse(result.isPresent());
  }
}