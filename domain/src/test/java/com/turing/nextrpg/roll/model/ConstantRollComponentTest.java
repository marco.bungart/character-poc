package com.turing.nextrpg.roll.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class ConstantRollComponentTest extends RollComponentTest<ConstantRollComponent> {

  @Override
  ConstantRollComponent createUnderTest() {
    return ConstantRollComponent.of(10);
  }

  @Test
  void shouldConstructExpectedConstant() {
    // GIVEN
    final int expectedValue = 6;

    // WHEN
    final ConstantRollComponent underTest = ConstantRollComponent.of(expectedValue);

    // THEN
    assertEquals(expectedValue, underTest.value());
  }

  @Test
  void shouldReturnTrueWhenComparedToSelf() {
    // GIVEN
    final ConstantRollComponent underTest = ConstantRollComponent.of(6);

    // WHEN
    final boolean isEqual = underTest.equals(underTest);

    // THEN
    assertTrue(isEqual);
  }

  @Test
  void shouldReturnTrueWhenComparedToEqualObject() {
    // GIVEN
    final ConstantRollComponent underTest = ConstantRollComponent.of(6);
    final ConstantRollComponent equalRollComponent = ConstantRollComponent.of(6);

    // WHEN
    final boolean isEqual = underTest.equals(equalRollComponent);

    // THEN
    assertTrue(isEqual);
  }

  @Test
  void shouldReturnFalseWhenComparedToNull() {
    // GIVEN
    final ConstantRollComponent component = ConstantRollComponent.of(6);

    // WHEN
    final boolean isEqual = component.equals(null);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnFalseWhenComparedToDifferentObjectType() {
    // GIVEN
    final ConstantRollComponent underTest = ConstantRollComponent.of(6);
    final Object differentTypeObject = new Object();

    // WHEN
    final boolean isEqual = underTest.equals(differentTypeObject);

    // THEN
    assertFalse(isEqual);
  }

  @Test
  void shouldReturnFalseWhenComparedToUnequalComponent() {
    // GIVEN
    final ConstantRollComponent underTest = ConstantRollComponent.of(6);
    final ConstantRollComponent differentConstantRollComponent = ConstantRollComponent.of(4);

    // WHEN
    final boolean isEqual = underTest.equals(differentConstantRollComponent);

    // THEN
    assertFalse(isEqual);
  }


  @Test
  void shouldReturnSameHashCodeWhenCalledOnEqualComponent() {
    // GIVEN
    final ConstantRollComponent underTestOne = ConstantRollComponent.of(6);
    final ConstantRollComponent equalConstantRollComponent = ConstantRollComponent.of(6);

    // WHEN
    final long hashCodeOne = underTestOne.hashCode();
    final long hashCodeTwo = equalConstantRollComponent.hashCode();

    // THEN
    assertEquals(hashCodeOne, hashCodeTwo);
  }

  @Test
  void shouldReturnDifferentHashCodeWhenCalledOnUnequalComponent() {
    // GIVEN
    final ConstantRollComponent underTest = ConstantRollComponent.of(6);
    final ConstantRollComponent differentRollComponent = ConstantRollComponent.of(4);

    // WHEN
    final long hashCodeOne = underTest.hashCode();
    final long hashCodeTwo = differentRollComponent.hashCode();

    // THEN
    assertNotEquals(hashCodeOne, hashCodeTwo);
  }
}